# zfs-diff-move

Script to replicate moves reported by zfs diff. Note this is a
potentially destructive operation if the `--clobber` option is provided.

```
USAGE: zfs-diff-move.sh [--clobber] dir zfs-dir zfsdataset@old-snap [[zfsdataset]@new-snap]
```

Uses [`zfs diff`](https://openzfs.github.io/openzfs-docs/man/master/8/zfs-diff.8.html)
to find what files have been moved on the ZFS dataset `zfsdataset`
since the snapshot `@old-snap` (until `@new-snap` or the present if
not specified). `zfs-dir/` should be the mountpoint of `zfsdataset`
or a subdirectory of it. All file moves like `zfs-dir/some/path` to
`zfs-dir/another/name` will have the equivalent file move applied to the
directory structure under `dir/`: `dir/some/path` to `dir/another/name`.

**IMPORTANT:** Using the `--clobber` option can lead to data loss
if `dir/another/name` already exists. Otherwise (or if the default
`--no-clobber` is explicitly specified), moves that would overwrite will
instead be skipped.

`zfs-diff-move-small.sh` does the same thing but is written more tersely
and handles fewer edge cases in order to squeeze into the text of a blog
post.
